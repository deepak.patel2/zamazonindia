const data = {
  items: [
    {
      id: '1',
      name: 'MI A3',
      price: 14000,
      image: 'images/img1.jpg',
    },
    {
      id: '2',
      name: 'Blutooth',
      price: 2400,
      image: 'images/img2.jpg',
    },
    {
      id: '3',
      name: 'Speaker',
      price: 700,
      image: 'images/img3.jpg',
    },
    {
      id: '4',
      name: 'HP Pendrives',
      price: 3000,
      image: 'images/img4.jpg',
    },
    {
      id: '5',
      name: 'keyboard',
      price: 1000,
      image: 'images/img5.jpg',
    },
    {
      id: '6',
      name: 'Z Keyboard',
      price: 600,
      image: 'images/img6.jpg',
    },
    {
      id: '7',
      name: 'W Watch',
      price: 1000,
      image: 'images/img7.jpg',
    },
    {
      id: '8',
      name: 'MI Band',
      price: 1000,
      image: 'images/img8.jpg',
    },
    {
      id: '9',
      name: 'Apple Watch',
      price: 1000,
      image: 'images/img9.jpg',
    },
    {
      id: '10',
      name: 'SONY Headphone',
      price: 10000,
      image: 'images/img10.jpg',
    },
   
  ],
};
export default data;
