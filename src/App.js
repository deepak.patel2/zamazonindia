import Header from "./components/Header";
import Home from "./components/Home";
import Cart from "./components/Cart";
import data from "./data";
import { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Mobile from "./components/Mobile";

const App = () => {
  const { items } = data;
  const [cartItems, setCartItems] = useState([]);
  const onAdd = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if (exist) {
      setCartItems(
        cartItems.map((x) =>
          x.id === product.id ? { ...exist, qty: exist.qty + 1 } : x
        )
      );
    } else {
      setCartItems([...cartItems, { ...product, qty: 1 }]);
    }
  };
  const onRemove = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if (exist.qty === 1) {
      setCartItems(cartItems.filter((x) => x.id !== product.id));
    } else {
      setCartItems(
        cartItems.map((x) =>
          x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x
        )
      );
    }
  };
  return (
    <Router>
      <div className="App">
        <Header countCartItems={cartItems.length} items={items}></Header>

        <Routes>
          <Route exact path="/Mobile" element={<Mobile />} />

          <Route
            exact
            path="/Home"
            element={
              <div className="row">
                <Cart cartItems={cartItems} onAdd={onAdd} onRemove={onRemove} />
                <Home items={items} onAdd={onAdd}></Home>
              </div>
            }
          />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
