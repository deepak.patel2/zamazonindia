import React from 'react';

const SearchProducts = (props) => {
  
      
  
  const { product } = props;
  return (
    <div style={{float:"left",width:"20%"}}>
      <img style={{height : "100px",width : "100px"}} src={product.image} alt={product.name} />
      <h3>{product.name}</h3>
      <div>Rs{product.price}/-</div>
      
    </div>
  );
}

export default SearchProducts;
