import React from 'react';
import Product from './Product';

const Home = props => {
  //console.log(props)
  const { items, onAdd } = props;
  return (
    <main className="block1 col-2">
      <h1 style={{ color: "black",textAlign:"center" }}>Products</h1>
      <div className="row">
        {items.map((product) => (
          <Product key={product.id} product={product} onAdd={onAdd}></Product>
        ))}
      </div>
    </main>
  );
}

export default Home;
