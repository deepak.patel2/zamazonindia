import React from "react";
import { Link } from "react-router-dom";
import SearchBar from "./SearchBar";
//import Mobile from "./Mobile";
import {useNavigate} from "react-router-dom";


const Header = (props) => {
  //console.log(props);
  let navigate = useNavigate();
  return (
    <header className="block row center">
      <div>
        <h1 style={{ fontFamily: "cursive" }}>Zamazon India</h1>
       
      </div>
      <a href="/Mobile"><h3>Mobile</h3></a>
     
      <Link to="/Home"><h3>Products Manue</h3></Link>
      <button style={{width: "100px"}} onClick={() => {navigate("/Header")}}>Go to home</button>
      <SearchBar items={props.items}/>
      <div>
        <a href="/cart">
          Cart{" "}
          {props.countCartItems ? (
            <button className="badge">{props.countCartItems}</button>
          ) : (
            ""
          )}
        </a>{" "}
      </div>
    </header>
  );
};

export default Header;

